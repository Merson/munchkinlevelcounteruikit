//
//  ImageLabelView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit

class ImageLabelView: UIImageView {

    private let label = UILabel()

    public override init(image: UIImage?) {
        super.init(image: image)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(label)

        label.textColor = .red
    }

    private func initConstraints() {
        label.snp.makeConstraints { (make) in
            make.centerX.centerY.equalToSuperview()
        }
    }

    public func fill(image: UIImage?, text: String?) {
        self.image = image
        self.label.text = text
    }
}
