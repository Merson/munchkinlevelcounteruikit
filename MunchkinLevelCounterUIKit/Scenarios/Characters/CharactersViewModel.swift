//
//  ViewModel.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CharactersViewModel: ViewModel {
    
    public let charactersRelay = BehaviorRelay<[Character]>(value: [])
    public let characterCreationSubject = PublishSubject<Void>()
    public let characterDetailsSubject = PublishSubject<Character>()
    public let refreshCharactersSubject = PublishSubject<Void>()
    public let deleteCharacterRelay = PublishRelay<String>()
    
    private let characterApplicationLogic = CharacterApplicationLogic()
    
    override init() {
        super.init()
    }
    
    override func initRx() {
        super.initRx()
        refreshCharactersSubject
            .withUnretained(self)
            .map { $0.0.characterApplicationLogic.getCharacters() }
            .bind(to: charactersRelay)
            .disposed(by: disposeBag)
        
        deleteCharacterRelay
            .withUnretained(self)
            .do(onNext: { (me, name) in
                me.characterApplicationLogic.deleteCharacter(name: name)
            })
            .map { _ in return }
            .bind(to: refreshCharactersSubject).disposed(by: disposeBag)
    }
}
