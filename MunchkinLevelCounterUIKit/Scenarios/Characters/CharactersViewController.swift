//
//  ViewController.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa
import RxDataSources
import RxViewController
import RxGesture

class CharactersViewController: ViewController<CharactersViewModel> {

    private let segmentedControl = UISegmentedControl(items: ["Personnages", "Combat", "Options"])
    private let charactersView = CharactersView()

    override init(viewModel: CharactersViewModel) {
        super.init(viewModel: viewModel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func initUi() {
        super.initUi()
        view.addSubview(segmentedControl)
        view.addSubview(charactersView)

        title = "Munchkin"

        charactersView.fill(charactersRelay: viewModel.charactersRelay,
                            characterDetailsSubject: viewModel.characterDetailsSubject,
                            characterCreationSubject: viewModel.characterCreationSubject,
                            deleteCharacterRelay: viewModel.deleteCharacterRelay,
                            disposeBag: disposeBag)
    }

    override func initConstraints() {
        super.initConstraints()
        segmentedControl.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(view.safeAreaLayoutGuide)
        }
        charactersView.snp.makeConstraints { (make) in
            make.top.equalTo(segmentedControl.snp.bottom)
            make.left.right.bottom.equalTo(view.safeAreaLayoutGuide)
        }
    }

    override func initRx() {
        super.initRx()

        // On refresh la liste de personnage à chaque fois que la vue apparait
        rx.viewWillAppear.map { _ in return }.bind(to: viewModel.refreshCharactersSubject).disposed(by: disposeBag)

        segmentedControl.rx.selectedSegmentIndex.onNext(0)
        segmentedControl.rx.selectedSegmentIndex.bind { [weak self] (index) in
            guard let me = self else { return }
            UIView.animate(withDuration: 0.3) {
                switch index {
                case 0:
                    me.charactersView.alpha = 1
                case 1:
                    me.charactersView.alpha = 0
                case 2:
                    me.charactersView.alpha = 0
                default:
                    return
                }
            }
        }.disposed(by: disposeBag)
    }
}

extension CharactersViewController {
}
