//
//  CharacterStatView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit

class CharacterInfosView: UIView {

    private let infosImageView = UIImageView()
    private let infosLabel = UILabel()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(infosImageView)
        addSubview(infosLabel)

        infosLabel.font = .boldSystemFont(ofSize: 20)
        infosLabel.textColor = .white
    }

    private func initConstraints() {
        let infos = Settings.Display.infos
        let margin = Settings.Display.margin
        infosImageView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.width.height.equalTo(infos)
        }
        infosLabel.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.left.equalTo(infosImageView.snp.right).offset(margin)
        }
    }

    public func fill(image: UIImage?, infos: Int, color: UIColor? = .white) {
        infosImageView.image = image
        infosLabel.text = String(infos)
        infosLabel.textColor = color
    }

    public func clean() {
        infosImageView.image = nil
    }
}
