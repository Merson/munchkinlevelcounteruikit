//
//  CharacterTableViewCell.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit

class CharacterTableViewCell: TableViewCell {

    public let view = CharacterView()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUi()
        initConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initUi() {
        clipsToBounds = false
        contentView.addSubview(view)
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        view.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(margin)
            make.left.equalToSuperview().offset(margin)
            make.right.equalToSuperview().offset(-margin)
            make.bottom.equalToSuperview().offset(-margin)
        }
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        view.clean()
    }

    public func fill(character: Character) {
        view.fill(character: character)
    }
}
