//
//  CharacterView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit

class CharacterView: UIView {

    private let characterImageView = UIImageView()
    private let genderImageView = UIImageView()
    private let genderLabel = UILabel()
    private let nameLabel = UILabel()
    private let raceAndCareer = UILabel()

    private let stackView = UIStackView()
    private let levelInfosView = CharacterInfosView()
    private let gearInfosView = CharacterInfosView()
    private let bonusMalusView = CharacterInfosView()
    private let totalInfosView = CharacterInfosView()

    public var character: Character?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(characterImageView)
        addSubview(genderImageView)
        addSubview(genderLabel)
        addSubview(nameLabel)
        addSubview(raceAndCareer)
        addSubview(stackView)
        stackView.addArrangedSubview(levelInfosView)
        stackView.addArrangedSubview(gearInfosView)
        stackView.addArrangedSubview(bonusMalusView)
        stackView.addArrangedSubview(totalInfosView)

        nameLabel.numberOfLines = 2
        nameLabel.textAlignment = .center
        nameLabel.font = .boldSystemFont(ofSize: 30)
        nameLabel.textColor = .white

        raceAndCareer.numberOfLines = 2
        raceAndCareer.textAlignment = .center
        //raceAndCareer.font = .boldSystemFont(ofSize: 20)
        raceAndCareer.textColor = .white

        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        let infos = Settings.Display.infos
        characterImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalToSuperview()
            make.width.height.equalTo(infos * 2)
        }
        genderImageView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
            make.width.height.equalTo(infos)
        }
        nameLabel.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(margin)
            make.left.equalTo(characterImageView.snp.right).offset(margin)
            make.right.equalTo(genderImageView.snp.left).offset(-margin)
        }
        raceAndCareer.snp.makeConstraints { (make) in
            make.top.equalTo(nameLabel.snp.bottom)
            make.left.equalTo(characterImageView.snp.right).offset(margin)
            make.right.equalTo(genderImageView.snp.left).offset(-margin)
            make.bottom.lessThanOrEqualTo(characterImageView.snp.bottom)
        }
        stackView.snp.remakeConstraints { (make) in
            make.top.equalTo(characterImageView.snp.bottom).offset(margin)
            make.left.right.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }

    public func fill(character: Character) {
        self.character = character

        backgroundColor = .black

        characterImageView.image = character.characterImage
        genderImageView.image = character.gender.image

        nameLabel.text = character.name
        raceAndCareer.text = "\(character.race.rawValue) \(character.career.rawValue)"

        levelInfosView.fill(image: #imageLiteral(resourceName: "upgrade"), infos: character.level)
        gearInfosView.fill(image: #imageLiteral(resourceName: "battle-gear"), infos: character.gear)
        bonusMalusView.fill(image: #imageLiteral(resourceName: "potion-ball"), infos: character.bonus - character.malus)
        totalInfosView.fill(image: #imageLiteral(resourceName: "sword-clash"), infos: character.total, color: .red)
    }

    public func clean() {
        characterImageView.image = nil
        genderImageView.image = nil
    }
}
