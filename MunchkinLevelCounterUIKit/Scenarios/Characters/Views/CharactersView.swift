//
//  CharactersView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 12/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CharactersView: UIView {

    private let characterCreationButton = UIButton()
    private let charactersTableView = UITableView(frame: .zero, style: .plain)

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(characterCreationButton)
        addSubview(charactersTableView)

        characterCreationButton.setTitle("Ajouter un personnage", for: .normal)
        characterCreationButton.setTitleColor(.white, for: .normal)
        characterCreationButton.titleLabel?.font = .boldSystemFont(ofSize: 20)
        characterCreationButton.backgroundColor = .black

        charactersTableView.allowsSelection = true
        charactersTableView.separatorStyle = .none
        charactersTableView.estimatedRowHeight = 170
        charactersTableView.rowHeight = UITableView.automaticDimension
        charactersTableView.register(cellType: CharacterTableViewCell.self)
    }

    private func initConstraints() {
        characterCreationButton.snp.makeConstraints { (make) in
            make.left.bottom.right.equalToSuperview()
            make.height.equalTo(60)
        }
        charactersTableView.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
            make.bottom.equalTo(characterCreationButton.snp.top)
        }
    }

    public func fill(charactersRelay: BehaviorRelay<[Character]>,
                     characterDetailsSubject: PublishSubject<Character>,
                     characterCreationSubject: PublishSubject<Void>,
                     deleteCharacterRelay: PublishRelay<String>,
                     disposeBag: DisposeBag) {

        charactersTableView.dataSource = nil

        // On créer une cellule pour chaque personnage
        charactersRelay
            .observeOn(MainScheduler.instance)
            .bind(to: charactersTableView.rx.items(
                cellIdentifier: String(describing: CharacterTableViewCell.self),
                cellType: CharacterTableViewCell.self)) { row, data, cell in
                    cell.fill(character: data)
                    cell.rx.longPressGesture().when(.began).withUnretained(self).bind { (me, _) in
                        let alert = UIAlertController(title: "Voulez-vous supprimer ce personnage ?", message: nil, preferredStyle: .alert)

                        cell.setSelected(true, animated: true)

                        alert.addAction(UIAlertAction(title: "Non", style: .cancel, handler: { _ in
                            cell.setSelected(false, animated: true)
                        }))

                        alert.addAction(UIAlertAction(title: "Oui", style: .default, handler: { _ in
                            cell.setSelected(false, animated: true)
                            Observable
                                .of(cell.view.character?.name)
                                .unwrap()
                                .bind(to: deleteCharacterRelay)
                                .disposed(by: disposeBag)
                        }))

                        me.window?.rootViewController?.present(alert, animated: true)
                    }.disposed(by: cell.disposeBag)
        }.disposed(by: disposeBag)

        // On ouvre les détails du personnage au clic sur la cellule
        charactersTableView
            .rx
            .modelSelected(Character.self)
            .bind(to: characterDetailsSubject)
            .disposed(by: disposeBag)

        // On ouvre la création de personnage
        characterCreationButton
            .rx
            .tap
            .bind(to: characterCreationSubject)
            .disposed(by: disposeBag)
    }
}
