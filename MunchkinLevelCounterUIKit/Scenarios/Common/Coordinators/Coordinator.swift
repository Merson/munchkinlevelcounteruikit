//
//  Coordinator.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import RxSwift

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }
    func start()
}
