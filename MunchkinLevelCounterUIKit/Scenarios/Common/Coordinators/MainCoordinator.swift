//
//  MainCoordinator.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import RxSwift

class MainCoordinator: Coordinator {

    let disposeBag = DisposeBag()

    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        startCharacters()
    }

    func startCharacters() {
        let vm = CharactersViewModel()
        vm.characterCreationSubject.withUnretained(self).bind { (me, _) in
            me.startCharacterCreation()
        }.disposed(by: disposeBag)
        vm.characterDetailsSubject.withUnretained(self).bind { (me, character) in
            me.startCharacterDetails(character: character)
        }.disposed(by: disposeBag)
        let vc = CharactersViewController(viewModel: vm)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }

    func startCharacterCreation() {
        let vm = CharacterCreationViewModel()
        vm.characterCreationSubject.withUnretained(self).bind { (me, _) in
            me.navigationController.popViewController(animated: true)
        }.disposed(by: disposeBag)
        let vc = CharacterCreationViewController(viewModel: vm)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }

    func startCharacterDetails(character: Character) {
        let vm = CharacterDetailsViewModel(character: character)
        let vc = CharacterDetailsViewController(viewModel: vm)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
}
