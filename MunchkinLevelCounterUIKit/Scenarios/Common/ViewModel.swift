//
//  ViewModel.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import Foundation
import RxSwift

class ViewModel {
    public let disposeBag = DisposeBag()

    init() {
        initRx()
    }

    internal func initRx() {
    }
}
