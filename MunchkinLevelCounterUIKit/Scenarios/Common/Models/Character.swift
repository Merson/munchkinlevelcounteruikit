//
//  Character.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit

struct Character {
    var name: String
    var gender: GenderType
    var race: RaceType
    var career: CareerType
    var level: Int
    var gear: Int
    var bonus: Int
    var malus: Int

    var characterImage: UIImage?
    var color: UIColor?

    var total: Int {
        return level + gear + bonus - malus
    }
}
