//
//  LogicCharacter.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import Foundation
import UIKit
import Realm
import RealmSwift

var characters: [UIImage] = [#imageLiteral(resourceName: "visored-helm"), #imageLiteral(resourceName: "dwarf-helmet"), #imageLiteral(resourceName: "wizard-face"), #imageLiteral(resourceName: "brutal-helm"), #imageLiteral(resourceName: "barbarian"), #imageLiteral(resourceName: "woman-elf-face"), #imageLiteral(resourceName: "warlock-hood"), #imageLiteral(resourceName: "barbute")]
var colors: [UIColor] = [.red, .green, .yellow, .purple, .blue]

class LogicCharacter: Object {

    override static func primaryKey() -> String? {
        return "name"
    }

    @objc dynamic var name: String = ""
    @objc dynamic var gender: String = GenderType.male.rawValue
    @objc dynamic var race: String = RaceType.human.rawValue
    @objc dynamic var career: String = CareerType.none.rawValue
    @objc dynamic var level: Int = 1
    @objc dynamic var gear: Int = 0
    @objc dynamic var bonus: Int = 0
    @objc dynamic var malus: Int = 0

    @objc dynamic var characterImage: Data? = characters.shuffled().first?.pngData()
    //var characterImage: UIImage? = characters.shuffled().first
    var color: UIColor? = colors.shuffled().first

     init(name: String, gender: String) {
        self.name = name
        self.gender = gender
    }

    override required init() {
    }

    var character: Character {
        return Character(name: name,
                         gender: GenderType(rawValue: gender) ?? .male,
                         race: RaceType(rawValue: race) ?? .human,
                         career: CareerType(rawValue: career) ?? .none,
                         level: level,
                         gear: gear,
                         bonus: bonus,
                         malus: malus,
                         characterImage: UIImage(data: characterImage ?? Data()) ?? #imageLiteral(resourceName: "visored-helm"),
                         color: color)
    }
}
