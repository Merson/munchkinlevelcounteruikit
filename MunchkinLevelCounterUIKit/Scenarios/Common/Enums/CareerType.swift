//
//  CareerType.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import Foundation
import UIKit

enum CareerType: String {
    case none = "sans classe"
    case warrior = "Guerrier"
    case priest = "Prêtre"
    case thief = "Voleur"
    case mage = "Mage"
    case bard = "Barde"
    case cultist = "Cultiste"
    case professor = "Professeur"
    case monsterKiller = "Tabasseur de monstres"
    case investigator = "Investigateur"

    public var all: [String] {
        return [CareerType.none.rawValue,
                CareerType.warrior.rawValue,
                CareerType.priest.rawValue,
                CareerType.thief.rawValue,
                CareerType.mage.rawValue,
                CareerType.bard.rawValue,
                CareerType.cultist.rawValue,
                CareerType.professor.rawValue,
                CareerType.monsterKiller.rawValue,
                CareerType.investigator.rawValue]
    }
}
