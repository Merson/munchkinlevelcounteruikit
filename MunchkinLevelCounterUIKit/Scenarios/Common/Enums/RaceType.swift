//
//  RaceType.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import Foundation

enum RaceType: String {
    case human = "Humain"
    case elven = "Elfe"
    case dwarf = "Nain"
    case halfling = "Halfelin"
    case orc = "Orc"
    case gnome = "Gnome"

    public var all: [String] {
        return [RaceType.human.rawValue,
                RaceType.elven.rawValue,
                RaceType.dwarf.rawValue,
                RaceType.halfling.rawValue,
                RaceType.orc.rawValue,
                RaceType.gnome.rawValue]
    }
}
