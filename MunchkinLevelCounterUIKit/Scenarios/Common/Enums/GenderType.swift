//
//  GenderType.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit

enum GenderType: String {
    case male = "Mâle"
    case female = "Femelle"
}

extension GenderType {
    var image: UIImage? {
        switch self {
        case .male:
            return #imageLiteral(resourceName: "male")
        case .female:
            return #imageLiteral(resourceName: "female")
        }
    }

    var isOn: Bool {
        switch self {
        case .male:
            return false
        case .female:
            return true
        }
    }
}
