//
//  Settings.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 03/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import Foundation

class Settings {

    struct Display {
        static var margin = 10
        static var infos = 40
    }
}
