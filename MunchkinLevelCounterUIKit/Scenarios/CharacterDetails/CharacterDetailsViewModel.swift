//
//  CharactersDetailsViewModel.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CharacterDetailsViewModel: ViewModel {

    public let character: Character

    public let genderSubject = PublishSubject<String>()

    public let raceSubject = PublishSubject<String>()
    public let careerSubject = PublishSubject<String>()

    public let levelSubject = PublishSubject<Int>()
    public let gearSubject = PublishSubject<Int>()
    public let bonusSubject = PublishSubject<Int>()
    public let malusSubject = PublishSubject<Int>()
    public let characterUpdateSubject = PublishSubject<Void>()

    private let characterApplicationLogic = CharacterApplicationLogic()

    init(character: Character) {
        self.character = character
        super.init()
    }

    override func initRx() {
        super.initRx()
        let fields = Observable.combineLatest(genderSubject, raceSubject, careerSubject, levelSubject, gearSubject, bonusSubject, malusSubject)
        characterUpdateSubject.withLatestFrom(fields).withUnretained(self).bind { (me, fields) in
            me.characterApplicationLogic.updateCharacter(name: me.character.name,
                                                         gender: fields.0,
                                                         race: fields.1,
                                                         career: fields.2,
                                                         level: fields.3,
                                                         gear: fields.4,
                                                         bonus: fields.5,
                                                         malus: fields.6)
        }.disposed(by: disposeBag)
    }
}
