//
//  CharactersDetailsViewController.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa
import RxViewController

class CharacterDetailsViewController: ViewController<CharacterDetailsViewModel> {

    private let scrollView = UIScrollView()
    private let characterDetailsView = CharacterDetailsView()

    override init(viewModel: CharacterDetailsViewModel) {
        super.init(viewModel: viewModel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func initUi() {
        super.initUi()
        view.addSubview(scrollView)
        scrollView.addSubview(characterDetailsView)

        title = viewModel.character.name

        characterDetailsView.fill(character: viewModel.character,
                                  genderSubject: viewModel.genderSubject,
                                  raceSubject: viewModel.raceSubject,
                                  careerSubject: viewModel.careerSubject,
                                  levelSubject: viewModel.levelSubject,
                                  gearSubject: viewModel.gearSubject,
                                  bonusSubject: viewModel.bonusSubject,
                                  malusSubject: viewModel.malusSubject,
                                  disposeBag: disposeBag)
    }

    override func initConstraints() {
        super.initConstraints()
        scrollView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalTo(view.safeAreaLayoutGuide)
        }
        characterDetailsView.snp.makeConstraints { (make) in
            make.top.equalTo(scrollView)
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.bottom.equalTo(scrollView)
        }
    }

    override func initRx() {
        super.initRx()
        rx.viewWillDisappear.map { _ in return }.bind(to: viewModel.characterUpdateSubject).disposed(by: disposeBag)
    }
}
