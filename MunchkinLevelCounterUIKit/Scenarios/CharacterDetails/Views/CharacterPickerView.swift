//
//  CharacterPickerView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 06/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxSwiftExt

class CharacterPickerView: UIView {

    private let label = UILabel()
    private let pickerView = UIPickerView()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(label)
        addSubview(pickerView)

        backgroundColor = .black

        label.textColor = .white
        label.textAlignment = .center
    }

    private func initConstraints() {
        //let infos = Settings.Display.infos
        label.snp.makeConstraints { (make) in
            make.top.left.right.equalToSuperview()
        }
        pickerView.snp.makeConstraints { (make) in
            make.top.equalTo(label.snp.bottom)
            make.left.right.bottom.equalToSuperview()
        }
    }

    public func fill(text: String,
                     element: String,
                     elements: [String],
                     elementSubject: PublishSubject<String>,
                     disposeBag: DisposeBag) {
        label.text = text

        Observable
            .just(elements)
            .bind(to: pickerView.rx.itemAttributedTitles) { (row, element) in
                return NSAttributedString(string: element, attributes: [
                    NSAttributedString.Key.foregroundColor: UIColor.white,
                ])
        }.disposed(by: disposeBag)

        pickerView
            .rx
            .modelSelected(String.self)
            .map { $0.first }
            .unwrap()
            .bind(to: elementSubject)
            .disposed(by: disposeBag)

        elementSubject.onNext(element)

        if let index = elements.firstIndex(of: element) {
            pickerView.selectRow(index, inComponent: 0, animated: true)
        }
    }
}
