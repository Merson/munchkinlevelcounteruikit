//
//  CharacterTotalView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 07/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class CharacterTotalView: UIView {

    private let totalImageView = UIImageView(image: #imageLiteral(resourceName: "sword-clash"))
    private let totalLabel = UILabel()

    private let resultLabel = UILabel()

    private let monsterImageView = UIImageView(image: #imageLiteral(resourceName: "spiked-dragon-head"))
    private let monsterTextField = UITextField()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(totalImageView)
        addSubview(totalLabel)
        addSubview(resultLabel)
        addSubview(monsterImageView)
        addSubview(monsterTextField)

        totalLabel.textAlignment = .center
        totalLabel.backgroundColor = .black
        totalLabel.textColor = .white

        resultLabel.backgroundColor = .black
        resultLabel.textColor = .white
        resultLabel.textAlignment = .center

        monsterTextField.attributedPlaceholder = NSAttributedString(string: "Monstre", attributes: [
            .foregroundColor: UIColor.white
        ])
        monsterTextField.keyboardType = .numberPad
        monsterTextField.textAlignment = .center
        monsterTextField.backgroundColor = .black
        monsterTextField.textColor = .white
        monsterTextField.tintColor = .white
    }

    private func initConstraints() {
        let infos = Settings.Display.infos
        totalImageView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.width.height.equalTo(infos * 2)
        }
        totalLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(totalImageView.snp.right)
            make.width.equalTo(infos)
        }
        monsterImageView.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.width.height.equalTo(infos * 2)
        }
        monsterTextField.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.right.equalTo(monsterImageView.snp.left)
            make.width.equalTo(infos * 2)
        }
        resultLabel.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(totalLabel.snp.right).priority(.medium)
            make.right.equalTo(monsterTextField.snp.left).priority(.medium)
        }
    }

    public func fill(character: Character,
                     careerSubject: PublishSubject<String>,
                     levelSubject: PublishSubject<Int>,
                     gearSubject: PublishSubject<Int>,
                     bonusSubject: PublishSubject<Int>,
                     malusSubject: PublishSubject<Int>,
                     disposeBag: DisposeBag) {

        let materializedTotal = Observable
            .combineLatest(levelSubject,
                           gearSubject,
                           bonusSubject,
                           malusSubject)
            .map { $0.0 + $0.1 + $0.2 - $0.3 }
            .materialize()
            .share()

        let materializedMonster = monsterTextField
            .rx
            .text
            .unwrap()
            .map { Int($0) }
            .unwrap()
            .materialize()
            .share()

        Observable
            .combineLatest(materializedTotal.elements(),
                           materializedMonster.elements(),
                           careerSubject)
            .map { ($0.0 - $0.1, $0.2) }.withUnretained(self).map { (me, arg1) -> String in
                let (total, career) = arg1
                if total > 0 || (total == 0 && CareerType(rawValue: career) == .warrior) {
                    me.resultLabel.textColor = .green
                    return "Victoire !"
                }
                me.resultLabel.textColor = .red
                return "Défaite..."
        }.bind(to: resultLabel.rx.text).disposed(by: disposeBag)

        materializedTotal
            .elements()
            .map { String($0) }
            .bind(to: totalLabel.rx.text)
            .disposed(by: disposeBag)
        careerSubject.onNext(character.career.rawValue)
        levelSubject.onNext(character.level)
        gearSubject.onNext(character.gear)
        bonusSubject.onNext(character.bonus)
        malusSubject.onNext(character.malus)
    }
}
