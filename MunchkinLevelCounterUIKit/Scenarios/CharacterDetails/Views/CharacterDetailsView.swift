//
//  CharacterDetailsView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxSwiftExt

class CharacterDetailsView: UIView {
    
    private let characterImageView = UIImageView()
    private let characterGenderView = CharacterGenderView()
    
    private let stackView = UIStackView()
    private let raceCareerPickerView = CharacterRaceCareerPickerView()
    private let levelSelectorView = CharacterNumberSelectorView()
    private let gearSelectorView = CharacterNumberSelectorView()
    private let bonusSelectorView = CharacterNumberSelectorView()
    private let malusSelectorView = CharacterNumberSelectorView()
    private let characterTotalView = CharacterTotalView()
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        initUi()
        initConstraints()
    }
    
    private func initUi() {
        addSubview(characterImageView)
        addSubview(characterGenderView)
        addSubview(stackView)
        stackView.addArrangedSubview(raceCareerPickerView)
        stackView.addArrangedSubview(levelSelectorView)
        stackView.addArrangedSubview(gearSelectorView)
        stackView.addArrangedSubview(bonusSelectorView)
        stackView.addArrangedSubview(malusSelectorView)
        stackView.addArrangedSubview(characterTotalView)
                
        let margin = Settings.Display.margin
        stackView.axis = .vertical
        stackView.distribution = .fillProportionally
        stackView.spacing = CGFloat(margin)
    }
    
    private func initConstraints() {
        let margin = Settings.Display.margin
        let infos = Settings.Display.infos
        characterImageView.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview()
            make.width.height.equalTo(infos * 2)
        }
        characterGenderView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.right.equalToSuperview()
        }
        stackView.snp.makeConstraints { (make) in
            make.top.equalTo(characterImageView.snp.bottom).offset(margin)
            make.left.right.equalToSuperview()
            make.bottom.lessThanOrEqualToSuperview().priority(.high)
        }
    }
    
    public func fill(character: Character,
                     genderSubject: PublishSubject<String>,
                     raceSubject: PublishSubject<String>,
                     careerSubject: PublishSubject<String>,
                     levelSubject: PublishSubject<Int>,
                     gearSubject: PublishSubject<Int>,
                     bonusSubject: PublishSubject<Int>,
                     malusSubject: PublishSubject<Int>,
                     disposeBag: DisposeBag) {
        characterImageView.image = character.characterImage
        
        characterGenderView.fill(character: character,
                                 genderSubject: genderSubject,
                                 disposeBag: disposeBag)
        
        raceCareerPickerView.fill(character: character,
                                  raceSubject: raceSubject,
                                  careerSubject: careerSubject,
                                  disposeBag: disposeBag)
        
        levelSelectorView.fill(number: character.level,
                               image: #imageLiteral(resourceName: "upgrade"),
                               text: "Niveau",
                               numberSubject: levelSubject,
                               disposeBag: disposeBag)
        gearSelectorView.fill(number: character.gear,
                              image: #imageLiteral(resourceName: "battle-gear"),
                              text: "Equipement",
                              numberSubject: gearSubject,
                              disposeBag: disposeBag)
        bonusSelectorView.fill(number: character.bonus,
                               image: #imageLiteral(resourceName: "potion-ball"),
                               text: "Bonus",
                               numberSubject: bonusSubject,
                               disposeBag: disposeBag)
        malusSelectorView.fill(number: character.malus,
                               image: #imageLiteral(resourceName: "poison-bottle"),
                               text: "Malus",
                               numberSubject: malusSubject,
                               disposeBag: disposeBag)
        characterTotalView.fill(character: character,
                                careerSubject: careerSubject,
                                levelSubject: levelSubject,
                                gearSubject: gearSubject,
                                bonusSubject: bonusSubject,
                                malusSubject: malusSubject,
                                disposeBag: disposeBag)
    }
}
