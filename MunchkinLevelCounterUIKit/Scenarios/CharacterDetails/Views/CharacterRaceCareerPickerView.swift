//
//  CharacterRaceCareerPicker.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 06/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxSwiftExt

class CharacterRaceCareerPickerView: UIView {

    private let stackView = UIStackView()
    private let racePickerView = CharacterPickerView()
    private let careerPickerView = CharacterPickerView()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(stackView)
        stackView.addArrangedSubview(racePickerView)
        stackView.addArrangedSubview(careerPickerView)

        backgroundColor = .white

        let margin = Settings.Display.margin
        stackView.axis = .horizontal
        stackView.distribution = .fill
        //stackView.spacing = CGFloat(margin)
    }

    private func initConstraints() {
        stackView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
    }

    public func fill(character: Character,
                     raceSubject: PublishSubject<String>,
                     careerSubject: PublishSubject<String>,
                     disposeBag: DisposeBag) {
        racePickerView.fill(text: "Choix de la race",
                            element: character.race.rawValue,
                            elements: RaceType.dwarf.all,
                            elementSubject: raceSubject,
                            disposeBag: disposeBag)
        careerPickerView.fill(text: "Choix de la classe",
                              element: character.career.rawValue,
                              elements: CareerType.cultist.all,
                              elementSubject: careerSubject,
                              disposeBag: disposeBag)
    }
}
