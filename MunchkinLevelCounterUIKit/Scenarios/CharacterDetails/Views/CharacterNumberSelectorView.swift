//
//  CharacterNumberSelectorView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxSwiftExt

class CharacterNumberSelectorView: UIView {

    private let imageView = UIImageView()
    private let label = UILabel()
    private let removeButton = UIButton()
    private let displayLabel = UILabel()
    private let addButton = UIButton()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(imageView)
        addSubview(label)
        addSubview(removeButton)
        addSubview(displayLabel)
        addSubview(addButton)

        backgroundColor = .black

        label.textColor = .white

        removeButton.setTitle("-", for: .normal)
        removeButton.setTitleColor(.white, for: .normal)

        displayLabel.textColor = .white
        displayLabel.textAlignment = .center

        addButton.setTitle("+", for: .normal)
        addButton.setTitleColor(.white, for: .normal)
    }

    private func initConstraints() {
        let margin = Settings.Display.infos
        let infos = Settings.Display.infos
        imageView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.width.height.equalTo(infos)
        }
        label.snp.makeConstraints { (make) in
            make.left.equalTo(imageView.snp.right).offset(margin)
            make.top.bottom.equalToSuperview()
        }
        addButton.snp.makeConstraints { (make) in
            make.top.right.bottom.equalToSuperview()
            make.width.height.equalTo(infos)
        }
        displayLabel.snp.makeConstraints { (make) in
            make.right.equalTo(addButton.snp.left).offset(-margin)
            make.top.bottom.equalToSuperview()
        }
        removeButton.snp.makeConstraints { (make) in
            make.right.equalTo(displayLabel.snp.left).offset(-margin)
            make.width.equalTo(infos)
        }
    }

    public func fill(number: Int,
                     image: UIImage?,
                     text: String?,
                     numberSubject: PublishSubject<Int>,
                     disposeBag: DisposeBag) {
        imageView.image = image
        label.text = text

        numberSubject
            .map { String($0) }
            .bind(to: displayLabel.rx.text)
            .disposed(by: disposeBag)

        removeButton
            .rx
            .tap
            .withLatestFrom(numberSubject)
            .map { $0 - 1 }
            .bind(to: numberSubject)
            .disposed(by: disposeBag)
        
        addButton
            .rx
            .tap
            .withLatestFrom(numberSubject)
            .map { $0 + 1 }
            .bind(to: numberSubject)
            .disposed(by: disposeBag)

        numberSubject.onNext(number)
    }
}
