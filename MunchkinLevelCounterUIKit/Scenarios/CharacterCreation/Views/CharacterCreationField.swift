//
//  CharacterCreationField.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit

class CharacterFieldView: UIView {

    private let stackView = UIStackView()
    private let imageView = UIImageView()
    private let label = UILabel()
    private let textField = UITextField()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(stackView)
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(textField)

        stackView.axis = .horizontal
        stackView.distribution = .fill

        textField.backgroundColor = .white

        backgroundColor = .yellow
    }

    private func initConstraints() {
        let infos = Settings.Display.infos
        stackView.snp.makeConstraints { (make) in
            make.top.left.right.bottom.equalToSuperview()
        }
        imageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(infos)
        }
        textField.snp.makeConstraints { (make) in
            make.width.height.equalTo(infos  * 2)
        }
    }

    public func fill(image: UIImage?, text: String?) {
        imageView.image = image
        label.text = text
    }
}
