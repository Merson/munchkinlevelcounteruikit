//
//  CharacterCreationView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxSwiftExt

class CharacterCreationView: UIView {

    private let stackView = UIStackView()
    private let nameField = CharacterFieldView()
    private let genderField = CharacterGenderView()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(stackView)
        stackView.addArrangedSubview(nameField)
        stackView.addArrangedSubview(genderField)

        let margin = Settings.Display.margin
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = CGFloat(margin)

        backgroundColor = .black
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        stackView.snp.makeConstraints { (make) in
            make.top.left.equalToSuperview().offset(margin)
            make.right.bottom.equalToSuperview().offset(-margin)
        }
    }

    public func fill(character: Character,
                     nameSubject: PublishSubject<String>,
                     genderSubject: PublishSubject<String>,
                     disposeBag: DisposeBag) {

        nameField.fill(image: #imageLiteral(resourceName: "barbute"), text: "Nom du personnage",
                       textSubject: nameSubject,
                       disposeBag: disposeBag)
        genderField.fill(character: character,
                         genderSubject: genderSubject,
                         disposeBag: disposeBag)
    }
}
