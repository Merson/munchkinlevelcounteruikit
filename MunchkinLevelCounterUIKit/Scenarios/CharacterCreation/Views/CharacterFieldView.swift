//
//  CharacterCreationField.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxCocoa
import RxSwiftExt

class CharacterFieldView: UIView {

    private let imageView = UIImageView()
    private let label = UILabel()
    private let textField = UITextField()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(imageView)
        addSubview(label)
        addSubview(textField)

        label.text = "Nom du personnage"
        label.textColor = .white
        label.sizeToFit()

        textField.backgroundColor = .white
    }

    private func initConstraints() {
        let margin = Settings.Display.margin
        let infos = Settings.Display.infos
        imageView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.width.height.equalTo(infos)
        }
        label.snp.makeConstraints { (make) in
            make.top.bottom.equalToSuperview()
            make.left.equalTo(imageView.snp.right).offset(margin)
        }
        textField.snp.makeConstraints { (make) in
            make.top.bottom.right.equalToSuperview()
            make.left.equalTo(label.snp.right).offset(margin)
        }
    }

    public func fill(image: UIImage?,
                     text: String?,
                     textSubject: PublishSubject<String>,
                     disposeBag: DisposeBag) {
        imageView.image = image
        label.text = text
        textField
            .rx
            .text
            .distinctUntilChanged()
            .unwrap()
            .bind(to: textSubject)
            .disposed(by: disposeBag)
    }
}
