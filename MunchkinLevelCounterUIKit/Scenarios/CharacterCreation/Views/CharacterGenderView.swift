//
//  CharacterGenderFieldView.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class CharacterGenderView: UIView {

    private let stackView = UIStackView()
    private let maleImageView = UIImageView(image: #imageLiteral(resourceName: "male"))
    private let switchView = UISwitch()
    private let femaleImageView = UIImageView(image: #imageLiteral(resourceName: "female"))

    public override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    private func commonInit() {
        initUi()
        initConstraints()
    }

    private func initUi() {
        addSubview(stackView)
        stackView.addArrangedSubview(maleImageView)
        stackView.addArrangedSubview(switchView)
        stackView.addArrangedSubview(femaleImageView)

        let margin = Settings.Display.margin
        stackView.axis = .horizontal
        stackView.distribution = .fill
        stackView.alignment = .center
        stackView.spacing = CGFloat(margin)

        switchView.overrideUserInterfaceStyle = .light
        switchView.onTintColor = .magenta
        switchView.thumbTintColor = .white
    }

    private func initConstraints() {
        let infos = Settings.Display.infos
        stackView.snp.makeConstraints { (make) in
            make.top.left.bottom.equalToSuperview()
            make.right.lessThanOrEqualToSuperview()
        }
        maleImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(infos)
        }
        femaleImageView.snp.makeConstraints { (make) in
            make.width.height.equalTo(infos)
        }
    }

    public func fill(character: Character,
                     genderSubject: PublishSubject<String>,
                     disposeBag: DisposeBag) {
        switchView.isOn = character.gender.isOn
        switchView
            .rx
            .value
            .map { $0 == true ? GenderType.female.rawValue : GenderType.male.rawValue }
            .bind(to: genderSubject)
            .disposed(by: disposeBag)
    }
}
