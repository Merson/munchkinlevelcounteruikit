//
//  CharacterCreationViewController.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CharacterCreationViewController: ViewController<CharacterCreationViewModel> {

    private let scrollView = UIScrollView()
    private let characterCreationView = CharacterCreationView()
    private let characterCreationButton = UIButton()

    override init(viewModel: CharacterCreationViewModel) {
        super.init(viewModel: viewModel)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func initUi() {
        super.initUi()
        view.addSubview(scrollView)
        view.addSubview(characterCreationButton)
        scrollView.addSubview(characterCreationView)

        characterCreationView.fill(character: LogicCharacter().character,
                                   nameSubject: viewModel.nameSubject,
                                   genderSubject: viewModel.genderSubject,
                                   disposeBag: disposeBag)

        title = "Ajouter un personnage"

        characterCreationButton.setTitle("Créer", for: .normal)
        characterCreationButton.setTitleColor(.white, for: .normal)
        characterCreationButton.titleLabel?.font = .boldSystemFont(ofSize: 20)
        characterCreationButton.backgroundColor = .black
    }

    override func initConstraints() {
        super.initConstraints()
        scrollView.snp.makeConstraints { (make) in
            make.top.left.right.equalTo(view.safeAreaLayoutGuide)
            make.bottom.equalTo(characterCreationButton.snp.top)
        }
        characterCreationButton.snp.makeConstraints { (make) in
            make.left.bottom.right.equalTo(view.safeAreaLayoutGuide)
            make.height.equalTo(60)
        }
        characterCreationView.snp.makeConstraints { (make) in
            make.top.equalToSuperview()
            make.left.equalTo(view)
            make.right.equalTo(view)
            make.bottom.equalToSuperview()
        }
    }

    override func initRx() {
        super.initRx()
        characterCreationButton
            .rx
            .tap
            .bind(to: viewModel.characterCreationSubject)
            .disposed(by: disposeBag)
    }
}
