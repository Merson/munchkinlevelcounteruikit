//
//  CharacterCreationViewModel.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 04/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import UIKit
import RxSwift
import RxSwiftExt
import RxCocoa

class CharacterCreationViewModel: ViewModel {

    public let characterCreationSubject = PublishSubject<Void>()

    public let nameSubject = PublishSubject<String>()
    public let genderSubject = PublishSubject<String>()

    private let characterApplicationLogic = CharacterApplicationLogic()

    override init() {
        super.init()
    }

    override func initRx() {
        super.initRx()
        let fields = Observable.combineLatest(nameSubject, genderSubject)
        characterCreationSubject
            .withLatestFrom(fields)
            .withUnretained(self).bind { (me, fields) in
                me.characterApplicationLogic.createCharacter(character: LogicCharacter(name: fields.0, gender: fields.1))
        }.disposed(by: disposeBag)
    }
}
