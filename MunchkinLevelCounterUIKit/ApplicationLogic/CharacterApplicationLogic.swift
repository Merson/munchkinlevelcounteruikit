//
//  CharacterApplicationLogic.swift
//  MunchkinLevelCounterUIKit
//
//  Created by Luc-Olivier MERSON on 05/06/2020.
//  Copyright © 2020 Luc-Olivier MERSON. All rights reserved.
//

import RxSwift
import RxSwiftExt
import RxCocoa
import Realm
import RealmSwift

class CharacterApplicationLogic {

    let realm = try! Realm()

    init() {

    }

    public func getCharacters() -> [Character] {
        return Array(realm.objects(LogicCharacter.self)).map { $0.character }
    }

    /*public func getCharacters() -> Single<[Character]> {
        return Single.just(Array(realm.objects(LogicCharacter.self)).map { $0.character })
    }*/

    public func createCharacter(character: LogicCharacter) {
        try! realm.write {
            realm.add(character)
        }
    }

    public func deleteCharacter(name: String) {
        try! realm.write {
            if let tmp = realm.object(ofType: LogicCharacter.self, forPrimaryKey: name) {
                realm.delete(tmp)
            }
        }
    }

    public func updateCharacter(name: String, gender: String, race: String, career: String, level: Int, gear: Int, bonus: Int, malus: Int) {
        if let tmp = realm.object(ofType: LogicCharacter.self, forPrimaryKey: name) {
            try! realm.write {
                tmp.gender = gender
                tmp.race = race
                tmp.career = career
                tmp.level = level
                tmp.gear = gear
                tmp.bonus = bonus
                tmp.malus = malus
                realm.add(tmp, update: .modified)
            }
        }
    }
}
